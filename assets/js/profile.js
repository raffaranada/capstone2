let token = localStorage.getItem("token");

let courseObjects = JSON.parse(localStorage.getItem("enrollments"));

let coursesId = courseObjects.map((element, index) => {   return courseObjects[index].courseId; });

let name = document.querySelector("#name");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");
let enrollContainer = document.querySelector("#enrollContainer");
let muid = localStorage.getItem("id");

/*console.log(localStorage.getItem("firstname"))*/
name.innerText =
  localStorage.getItem("firstname") + " " + localStorage.getItem("lastname");
email.innerText = localStorage.getItem("email");
mobileNo.innerText = localStorage.getItem("mobileNo");

let courseData;

if (coursesId == "" || coursesId == null) {
  enrollContainer.innerHTML = "Not enrolled in any of the classes";
} else {
  /*coursesId.forEach((element) => {*/
  courseObjects.forEach(async (element) => { //Add async to be able to use the await keyword on fetch this ensures that the function will wait till all promises are handled before proceeding
    let res_data = await fetch(`https://limitless-journey-41374.herokuapp.com/api/courses/${element.courseId}`) //Add await to make sure that the promise is waited before moving on to the next loop
        .then((res) => res.json()) // Parse the result  
        .then(data=>{ return data;}) // Add the return Data 
        .catch(err=>console.log(err))

        // console.log(res_data);
        for(var i = 0; i <= res_data.enrollees.length;i++)

        {
          if(res_data.enrollees[i].userId != muid) continue;
          console.log(res_data.enrollees[i].userId)
          return (enrollContainer.innerHTML += `<div class="card">
            <div class="card-body">
                <h5 class="card-title">Course</h5>
                <p class="card-text text-left">
                ${res_data.name}
                </p>
                <p class="card-text text-right">
                </p>
            </div>
            <div class="card-footer">Enrolled On:</div>
        </div>`);
        }
        
  });
}

  /*coursesId.map((element) => {
    fetch(`http://localhost:3000/api/courses/${element}`)*//*, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })*/
/*      .then((res) => {
        return res.json()*/
        /*console.log(res.json()) *//*return res.json();*/
/*      })
      .then(data=>console.log(data))
      .catch(err=>console.log(err))*/

/*      .then((data) => {
        .catch(err=>console.log(err))
        console.log(data)*/
     /*   return (enrollContainer.innerHTML += `<div class="card">
        <div class="card-body">
            <h5 class="card-title">Course</h5>
            <p class="card-text text-left">
            ${data.name}
            </p>
            <p class="card-text text-right">
            </p>
        </div>
        <div class="card-footer">Enrolled On:</div>
    </div>`);*/
      /*});*/
/*  });
}*/