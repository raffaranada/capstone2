//Exercise
//use the lesson for the enroll to archieve the course
//Steps
//1. Get the id from the URL

//To get the actual courseId, we need to use URL search params to access specific parts of the query

let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseID")
let token = localStorage.getItem("token") //cannot delete without admin logged in

//Q: How would I know if the request is from a user or an admin?

//2. Use the id as a parameter to the fetch and specify the URL

fetch(`https://limitless-journey-41374.herokuapp.com/api/courses/${courseId}`, {
	method : "DELETE",
	headers : {
		"Authorization" : `Bearer ${token}`
	}
})

.then(res => {return res.json()})
.then(data => {
	if (data) {
		alert ("Course Archived")
	} else {
		alert ("Something went wrong")
	}
})

//3. Process the data as needed
//4. Return to the courses page
/*
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseID")
let token = localStorage.getItem("token")
*/

/*let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")*/

/*let deleteContainer = document.querySelector("#deleteContainer") //not included

fetch(`http://localhost:3000/api/courses/${courseId}`)

.then(res => {return res.json()}) //enclosed in parenthesis
.then(data => {
	console.log(data)*/


/*	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price*/

	/*deleteContainer.innerHTML = `<button id = "deleteButton" class = "btn btn-block btn-danger">Delete</button>`
*/
	//after the delete button has been added, we add an event so that when the button is clicked, the course is deleted to a class

	/*document.querySelector("#deleteButton").addEventListener("click", () => {

		fetch(`http://localhost:3000/api/courses/${courseId}`, {
			method : "DELETE",
			headers : {
				"Content-Type" : "application/json",
				"Authorization" :`Bearer ${token}`
			},
			body : JSON.stringify ({
				courseId : courseId
			})
		})
		.then(res => { return res.json()})
		.then(data => {
			console.log(data)

			if (data) {
				alert("You have successfully deleted the course")
				window.location.replace("./deleteCourse.html")
			} else {
				alert("Deletion of a course failed")
			}
		})

	})

})*/