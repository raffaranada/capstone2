let loginForm = document.querySelector("#logInUser")
console.log(localStorage);

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	//If let below declared on top, it will execute on load of the page.
	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;

	// console.log(email)
	// console.log(password)

	if(email == "" || password == ""){
		alert("Please input your email and/or password")
	} else {
		fetch('https://limitless-journey-41374.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			// console.log(data)
			if(data !== false && data.accessToken !== null){
				localStorage.setItem('token', data.access);

				fetch('https://limitless-journey-41374.herokuapp.com/api/users/details', {
					headers: {
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
					//why we convert it to json? It is because we are dealing with javascript format
				})
				.then(data => {
					console.log(data)
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);
					//inserted new code blocks
					localStorage.setItem("firstname", data.firstname);
					localStorage.setItem("lastname", data.lastname);
					localStorage.setItem("email", data.email);
					localStorage.setItem("mobileNo", data.mobileNo);
					localStorage.setItem(
						"enrollments",
						JSON.stringify(data.enrollments)
						);


					console.log(localStorage)
					window.location.replace("./courses.html")

					/*console.log(data);*/
				})
			} else {
				alert("Something went wrong")
			}
		})
	}
})



/*ES6 function
() => {}

function function_name(parameters){
	//code block goes here
	//ex.
	return res.json()
}

*/


//localStorage
/*
localStorage = {
	token: data.accessToken
	setItem: function()
}
*/