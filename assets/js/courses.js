let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter; //use this to add a button at the bottom of each card to go to a specific course
//Added
let magic_variable = ['Profile']; // Magic Variable that you can add additional keywords that you want to filter or for whatever purpose you may use this for
let uri = 'https://limitless-journey-41374.herokuapp.com/api/courses/';

// console.log(adminUser)

if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;

} else {
	//Added
	document.querySelectorAll(".nav-link").forEach(ob => {
		for(var i=0; i <= magic_variable.length; i++)// Loop thru the magic variable to check if that specific keyword exists
		{
			if(ob.textContent.trim() ==  magic_variable[i])//Ensure that no white space is there 
			{
				ob.style.display = "none" ;//Add display none to hide the element
			}
		}
		
	});
	// uri =  'https://limitless-journey-41374.herokuapp.com/api/courses/all';
	// if( == "Profile") document.querySelector(".nav-item").style.display = "none" ;
	//--End of Added--
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>
		`
}

/*console.log(localStorage);*/

fetch('https://limitless-journey-41374.herokuapp.com/api/courses')//(uri)
.then(res => res.json())
.then(data => {
	console.log(data)
	let courseData;

	if(data.length < 1) {
		courseData = "No courses available";
	} else {
		courseData = data.map(course => { //break the array into a component elements
			//console.log(course._id)
			if(adminUser == "false" || !adminUser){
				cardFooter = `<a href="./course.html?courseID=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block">Go to Course</a>`
			} else {
				//Check if the user is an admin, and if they are, create a button that deletes the course
				cardFooter = `
				
				<a href="./deleteCourse.html?courseID=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block">Enable Course</a>

				<a href="./deleteCourse.html?courseID=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block">Archive Course</a>
				<div>${JSON.stringify(course.enrollees)}</div>
				`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">${course.name}</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
				`
			)		
		}).join("")
	}

	let container = document.querySelector("#coursesContainer");

	container.innerHTML = courseData;
})